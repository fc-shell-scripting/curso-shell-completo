# Summary

## Sobre o Curso

## Aula 01
* [Introdução](Aula01-Introducao/README.md#introdução)
    * [O que é o Bash?](Aula01-Introducao/README.md#o-que-é-o-bash)
    * [Relembrando alguns comandos](Aula01-Introducao/README.md#relembrando-alguns-comandos)
* [Editores](Aula01-Introducao/README.md#editores)
* [Tarefas para casa](Aula01-Introducao/README.md#tarefas-para-casa)
* [Desafio](Aula01-Introducao/README.md#desafio)

## Aula 02
* [CUT](Aula02-cut/README.md#cut)
    * [Forma de uso](Aula02-cut/README.md#forma-de-uso)
    * [Exercícios](Aula02-cut/README.md#exercícios)
    * [Desafio](Aula02-cut/README.md#desafio)

## Aula 03
* [Pipe](Aula03-Pipe/README.md#pipe)

## Aula 04
* [SED](Aula04-SED/README.md#sed)
    * [Expressões regulares](Aula04-SED/README.md#expressões-regulares)
    * [Uso básico do sed](Aula04-SED/README.md#uso-básico-do-sed)
    * [Uso de sed com regex simples](Aula04-SED/README.md#uso-de-sed-com-regex-simples)
    * [REGEX](Aula04-SED/README.md#Regex)
    * [Exercícios](Aula04-SED/README.md#exercícios)
    * [Desafio](Aula04-SED/README.md#desafio)

##Aula 05
* [Trabalho01](Aula05-Trabalho01/README.md)

##aula 06
* [AWK](Aula06-AWK/README.md#awk)
    * [Introdução](Aula06-AWK/README.md#introdução)
    * [AWK vs. CUT e SED](Aula06-AWK/README.md#awk-vs-cut-e-sed)
    * [Sintaxe](Aula06-AWK/README.md#sintaxe)
    * [Exercícios:](Aula06-AWK/README.md#exercícios)
    * [Desafio](Aula06-AWK/README.md#desafio)

## Aula 07
* [Introdução ao script](Aula07-Intro-Bash-IF/README.md#introdução-ao-script)
    * [Alguns detalhes antes de começarmos](Aula07-Intro-Bash-IF/README.md#alguns-detalhes-antes-de-começarmos)
    * [ALERTA!](Aula07-Intro-Bash-IF/README.md#alerta)
    * [Hello World](Aula07-Intro-Bash-IF/README.md#hello-world)
    * [Executando comandos mais complexos](Aula07-Intro-Bash-IF/README.md#executando-comandos-mais-complexos)
    * [IF](Aula07-Intro-Bash-IF/README.md#if)
    * [Exercícios](Aula07-Intro-Bash-IF/README.md#exercícios)
    * [Desafio](Aula07-Intro-Bash-IF/README.md#desafio)

## Aula 08
* [Read](Aula08-Read/README.md#read)
    * [Exemplo pronto](Aula08-Read/README.md#exemplo-pronto)
    * [Exercícios](Aula08-Read/README.md#exercícios)
    * [Desafio](Aula08-Read/README.md#desafio)

## Aula 09
* [While](Aula09-While/README.md#while)
    * [Introdução](Aula09-While/README.md#introdução)
    * [Break](Aula09-While/README.md#break)
    * [Exercícios](Aula09-While/README.md#exercícios)
    * [Desafios](Aula09-While/README.md#desafios)

##Aula 10
* [For](Aula10-For/README.md#for)
	* [Introdução](Aula10-For/README.md#introdução)
	* [For para varrer uma lista](Aula10-For/README.md#for-para-varrer-uma-lista)
	* [For usando contadores](Aula10-For/README.md#for-usando-contadores)
	* [Exercícios](Aula10-For/README.md#exercícios)
	* [Desafios](Aula10-For/README.md#desafios)

* [Select](Aula10-For-select/README.md#select)
    * [Exemplo básico](Aula10-For-select/README.md#exemplo-básico)
    * [Adicionando condição de saída](Aula10-For-select/README.md#adicionando-condição-de-saída)
    * [Personalizando a pergunta ao usuário](Aula10-For-select/README.md#personalizando-a-pergunta-ao-usuário)

##Aula 11
* [Funções](Aula11-funcoes-switch-case/README.md#funções)
    * [Funções com parâmetros](Aula11-funcoes-switch-case/README.md#funções-com-parâmetros)
* [Switch case](Aula11-funcoes-switch-case/README.md#switch-case)
* [Exercícios](Aula11-funcoes-switch-case/README.md#exercícios)

##Aula12
* [Conteúdo Bônus](Aula12-Conteudo-bonus/README.md#conteúdo-bônus)
    * [Printf](Aula12-Conteudo-bonus/README.md#printf)
        * [Printf com múltiplos argumentos](Aula12-Conteudo-bonus/README.md#printf-com-múltiplos-argumentos)
        * [Printf Com múltiplos dados no template](Aula12-Conteudo-bonus/README.md#printf-com-múltiplos-dados-no-template)
        * [formatação dos dados](Aula12-Conteudo-bonus/README.md#formatação-dos-dados)
    * [Operadores unários](Aula12-Conteudo-bonus/README.md#operadores-unários)